/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc tetses
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: Array<string>;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/auth/tetses`,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
