/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @description Auth Controller
 */
import check from "./check";
import login from "./login";
import tetses from "./tetses";

export class AuthApi {
  constructor(
    public check: (
      success?: ({
        data,
        ext,
        state,
        errors,
      }: {
        data: void;
        ext: ObjectMap;
        state: "SUCCESS" | "FAIL" | "EXCEPTION";
        errors?: Array<string>;
      }) => any,
      fail?: (error: string) => any
    ) => void,

    public login: (
      login: acl.Login,

      success?: ({
        data,
        ext,
        state,
        errors,
      }: {
        data: acl.AuthUser;
        ext: ObjectMap;
        state: "SUCCESS" | "FAIL" | "EXCEPTION";
        errors?: Array<string>;
      }) => any,
      fail?: (error: string) => any
    ) => void,

    public tetses: (
      success?: ({
        data,
        ext,
        state,
        errors,
      }: {
        data: Array<string>;
        ext: ObjectMap;
        state: "SUCCESS" | "FAIL" | "EXCEPTION";
        errors?: Array<string>;
      }) => any,
      fail?: (error: string) => any
    ) => void
  ) {}
}

export default {
  check,
  login,
  tetses,
} as AuthApi;
