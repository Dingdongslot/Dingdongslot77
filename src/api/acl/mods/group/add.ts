/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc add
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  group: acl.Group,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: acl.Group;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "post",
    url: `/group`,
    data: group,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
