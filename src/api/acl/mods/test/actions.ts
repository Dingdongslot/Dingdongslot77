/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc actions
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export class ActionsParams {
  constructor(public mids?: Array<number>) {}
}

export default async function (
  id: number,

  params: ActionsParams,
  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: acl.PermissionInfo;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/role/${id}/actions/info`,

    params,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
