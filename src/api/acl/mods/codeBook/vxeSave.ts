/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc vxeSave
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  vxeData: acl.VXETableSaveDTO<acl.CodeBook>,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: void;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "post",
    url: `/code/vxe/save`,
    data: vxeData,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
