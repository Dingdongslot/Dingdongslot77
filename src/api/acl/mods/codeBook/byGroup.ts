/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc byGroup
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  id: number,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: Array<acl.CodeBook>;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/group/${id}/codes`,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
